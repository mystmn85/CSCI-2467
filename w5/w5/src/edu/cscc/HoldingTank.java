package edu.cscc;

public class HoldingTank {
    private int current;
    private int maxCapacity;

    public HoldingTank() {
        current = 400; // number of gallons in the tank
        maxCapacity = 1200; // max number of gallons the tank can hold
    }

    public void add(int volume) {
        if (current + volume > maxCapacity) {
            current = maxCapacity;
        } else {
            current = current + volume;
        }
    }

    public void drain(int volume) {
        if (current - volume < 0) {
            current = 0;
        } else {
            current = current - volume;
        }
    }

    public void print() {
        System.out.println("The tank volume is " + current + " gallons");
    }
}
