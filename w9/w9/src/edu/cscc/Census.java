package edu.cscc;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Census {
    public static Surname[] loadCensusData(String fname) {
        int limit = 100;
        Surname[] namelist = new Surname[limit];

        // TODO - Add code to read file and populate array of Surname objects
        try {
            File file = new File(fname);
            Scanner sc = new Scanner(file).useDelimiter(",");

            if (file.canRead()) {
                int i = 0;
                while (sc.hasNext() && i < limit) {
                    namelist[i] = new Surname(sc.next(), sc.nextInt(), sc.nextInt(), sc.nextDouble());
                    //System.out.println("B:: " + i + " - " + sc.next() + " " + sc.nextInt() + " " + sc.nextInt() + " " + sc.nextDouble());
                    sc.nextLine();
                    i++;
//                    }
                }
                sc.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return namelist;
    }
}