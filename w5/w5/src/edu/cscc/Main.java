package edu.cscc;

/*
CSCI-2467 Lab 5 – Holding Tank Class
Paul Cameron
09/07/2020

This application takes the amount of water in a holding tank and calculates how much water is remaining after removing
and adding additional water. Starting with 400 gallons, it calculates add and substracting water from the tank.
The total amount of water the tank caps at is 1,200 gallons.
*/

public class Main {

    public static void main(String[] args) {
        HoldingTank myObj = new HoldingTank();
        myObj.print();
        myObj.add(400);
        myObj.drain(150);
        myObj.print();
        myObj.add(800);
        myObj.drain(250);
        myObj.print();
        myObj.drain(1200);
        myObj.add(400);
        myObj.drain(75);
        myObj.print();
    }
}