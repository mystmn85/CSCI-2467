package edu.cscc;

import java.util.Scanner;
import java.util.Arrays;

/*
Paul Cameron
09/07/2020

This application inputs phone prices from the user. It then sorts the most expensive phones and displays
the top three in descending order.

Secondly, it sorts the three cheapest phones and in ascending order, displays the top three cheapest prices.
*/

public class Main {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int[] prices = new int[10];
        int MaxNum = 10;

        System.out.println("Enter all smartphone prices: ");
        for (int i = 0; i < MaxNum; i++) {
            prices[i] = input.nextInt();
        }

        Arrays.sort(prices);

        System.out.println("Three Most Expensive Phones");
        for (int i = prices.length-1; i > prices.length-4; i--) {
            System.out.println(prices[i]);
        }

        System.out.println("\nThree Cheapest Phones");
        for (int i = 0; i < 3; i++) {
            System.out.println(prices[i]);
        }
    }
}