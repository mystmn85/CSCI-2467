package edu.cscc;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.HashMap;

public class Census {
    public static HashMap<String, Surname> loadCensusData(String fname) {
        HashMap<String, Surname> namelist = new HashMap<String, Surname>();

        try {
            File file = new File(fname);
            Scanner sc = new Scanner(file).useDelimiter(",");

            if (file.canRead()) {
                while (sc.hasNext()) {
                    String name = sc.next();
                    Integer rank = sc.nextInt();                   
                    Integer count = sc.nextInt();
                    Double proportion = sc.nextDouble();

                    Surname readFile = new Surname(name, rank, count, proportion);
                    namelist.put(name, readFile);
                    sc.nextLine();
                }
                sc.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return namelist;
    }
}