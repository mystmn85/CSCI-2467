package edu.cscc;

public class Surname {
    private String name;
    private int rank;
    private int count;
    private double proportion;

    public Surname(String name, int rank, int count, double proportion) {
        this.name = name;
        this.rank = rank;
        this.count = count;
        this.proportion = proportion;
    }

    //Getter
    public String getName() {
        return name;
    }

    //Setter
    public void setName(String name) {
        this.name = name;
    }

    //Getter
    public int getRank() {
        return rank;
    }

    //Setter
    public void setRank(int rank) {
        this.rank = rank;
    }

    //Getter
    public int getCount() {
        return count;
    }
    //Setter
    public void setCount(int count) {
        this.count = count;
    }
    
    //Getter
    public double getProportion() {
        return proportion;
    }
    //Setter
    public void setProportion(Double proportion) {
        this.proportion = proportion;
    }
}