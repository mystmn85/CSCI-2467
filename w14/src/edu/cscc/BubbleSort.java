package edu.cscc;

public class BubbleSort {
    public static void sort(String[] sortList) {
        boolean x;

        do {
            x = false;
            for (int i = 0; i<= sortList.length - 2; i++){
                if (sortList[i].compareTo(sortList[i+1])>0) {
                    String temp = sortList[i + 1];
                    sortList[i +1] = sortList[i];
                    sortList[i] = temp;
                    x = true;
                }
            }
        } while (x);
    }
}
