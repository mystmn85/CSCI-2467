package edu.cscc;

public class Node<T> {
    private Object content;
    private Node<T> next;

    public Node(Object content, Node<T> next) {
        this.content = content;
        this.next = next;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }
}
