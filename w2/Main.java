package edu.cscc;

import java.util.Scanner;

/*
Paul Cameron
9/2/2020
This application ask the user for wind speeds (in mph) and displays the category of that speed.
 */
public class Main {

    public static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        long speed;
        String classification;

        System.out.println("Enter wind speed (mph):");
        speed = input.nextLong();

        classification = "Invalid input.";

        if(speed >= 0 & speed <= 38) {
            classification = "Not in scale";
        }else if (speed >= 39 & speed <= 73) {
            classification = "Tropical storm";
        }else if (speed >= 74 & speed <= 95) {
            classification = "Category One Hurricane";
        }else if (speed >= 96 & speed <= 110) {
            classification = "Category Two Hurricane";
        }else if (speed >= 111 & speed <= 129) {
            classification = "Category Three Hurricane";
        }else if (speed >= 130 & speed <= 156) {
            classification = "Category Four Hurricane";
        }else if (speed >= 157) {
            classification = "Category Five Hurricane";
        }
        System.out.println("Classification: " + classification);
    }
}
