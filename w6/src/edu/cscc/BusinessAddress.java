package edu.cscc;

public class BusinessAddress extends Address {
    private String businessName;
    private String address2;

    //class constructor
    public BusinessAddress(String businessName, String address2, String streetAddress, String city, String state, String zip) {
        super(streetAddress, city, state, zip);
        this.businessName = businessName;
        this.address2 = address2;
    }

    //Getter
    public String getBusinessName() {
        return businessName;
    }

    //Getter
    public String getAddress2() {
        return address2;
    }

    //Setter
    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    //Setter
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public void printLabel() {
        System.out.println(
                this.businessName + "\n " + this.address2 + "\n");
    }
}
