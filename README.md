Fall of 2020 CSCI-2467 Java Class
====================================

This was an amazing class at Columbus State Community Colleage, learning the ways of Java. 

Assignment 1
-------
Asks the user for the radius and height of a cylinder. The application then prints the volume.

Assignment 2
-------
Asks the user for wind speeds (in mph) and displays the category of that speed.

Assignment 3
-------
Iputs the users weight in lbs and height in inches). It then converts the lbs to kgs and the inches
to meters and outputs the BMI for that individual.

Assignment 4
-------
Inputs phone prices from the user. It then sorts the most expensive phones and displays
the top three in descending order.

Secondly, it sorts the three cheapest phones and in ascending order, displays the top three cheapest prices.

Assignment 5
-------
Takes the amount of water in a holding tank and calculates how much water is remaining after removing
and adding additional water. Starting with 400 gallons, it calculates add and substracting water from the tank.

The total amount of water the tank caps at is 1,200 gallons.

Assignment 6
-------
Added two classes derived from two other classes. Displaying a label like system for shipping packages. 

Assignment 7
-------
Learned how to JavaDoc and add tags

Assignment 8
-------
We rewrote w3's lab and added validation to the user's input.

Assignment 9
-------
Java program to generate a report based on a .csv file


Assignment 10
-------
Learned about Comparators and Array.sort().

Assignment 11
-------
Learned about Java Generics, modified two pre-assigned files to be more Java Generic. 

Assignment 12
-------
Learned about processDocument() and Collection objects. This application reads a file one line at a time and store unique words into a  Collection object.

Assignment 13
-------
We rewrote lab w9 to use Map collections and HashMap. This application took the .csv file ask for a surname and diplayed the names row position.

Assignment 14
-------
Impemented Bubble Sort for sorting arrays of Strings. We used large datasets to compare to algorithms.
