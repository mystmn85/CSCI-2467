package edu.cscc;
import java.util.*;

// Bob Platt, 06/23/2017
// Calculate whether a year is a leap year
public class Main {

    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int year;
        String result;

        System.out.print("Leap Year Calculator\nPlease enter a year: ");
        year = input.nextInt();

        if ((year % 4 == 0) &&
                ((year % 100 != 0) || (year % 400 == 0))) {
            result = "";
        } else {
            result = "not ";
        }
        System.out.println("The year "+year+" is "+result+"a leap year");
    }
}
