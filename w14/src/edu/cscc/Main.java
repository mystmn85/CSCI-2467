package edu.cscc;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static String SERFNAME = "surnames.ser";
    private static Scanner sc = new Scanner(System.in);


    public static void main(String[] args) {

        String[] surnames;
        String userInput;
        do {
            System.out.println("Which file would you like to use?\n a) 100 names \n b) 25,000 names");
            userInput = sc.nextLine();
            if (userInput.equalsIgnoreCase("a")){
                SERFNAME = "surnames.ser";
            } else if (userInput.equalsIgnoreCase("b")){
                SERFNAME = "bigsurnames.ser";
            }
        } while (!(userInput.equalsIgnoreCase("a")|| userInput.equalsIgnoreCase("b")));

        try {
            surnames = CensusData.deserialize(SERFNAME);
            System.out.println("Unsorted array of "+surnames.length+" names");
            top5names(surnames);
            System.out.println("=========================");

            System.out.println("Sort array with Bubble Sort");
            long start = System.currentTimeMillis();

            BubbleSort.sort(surnames);

            long stop = System.currentTimeMillis();
            System.out.println("Elapsed time: "+(stop-start)+" milliseconds");
            top5names(surnames);
            System.out.println("=========================");

            surnames = CensusData.deserialize(SERFNAME);
            System.out.println("Sort array with Java built-in sort");
            start = System.currentTimeMillis();

            Arrays.sort(surnames);

            stop = System.currentTimeMillis();
            System.out.println("Elapsed time: "+(stop-start)+" milliseconds");
            top5names(surnames);
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("Cannot read file "+SERFNAME);
        }
    }

    public static void top5names(String[] names) {
        System.out.println("Top 5 names in list");
        for (int i=0; i<5; ++i) {
            System.out.println(names[i]);
        }
    }
}
