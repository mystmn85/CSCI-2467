package edu.cscc;
import java.lang.Math;
import java.util.Scanner;

/*
Paul Cameron
09/01/2020
This application asks the user for the radius and height of a cylinder. The app then prints the volume.
*/

public class Main {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        double radius, height, volume;

        System.out.println("Enter the radius of the cylinder: ");
        radius = input.nextDouble();

        System.out.println("Enter the height of the cylinder: ");
        height = input.nextDouble();

        volume = Math.PI * radius * radius * height;

        System.out.println("input was " + radius);
        System.out.println("input was " + height);
        System.out.println("The volume of the cylinder is: " + volume);
    }
}