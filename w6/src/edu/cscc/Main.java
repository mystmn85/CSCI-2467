package edu.cscc;

public class Main {

    public static void main(String[] args) {
        Address[] addressList = new Address[6];

        // TODO Add 3 person addresses to list
        addressList[0] = new PersonAddress("Saul Goodman", "a", "b", "c", "43230");

        // TODO Add 3 business address to list

        for (Address address : addressList) {
            address.printLabel();
            System.out.println("====================");
        }
    }
}
