package edu.cscc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.util.Set;
import java.util.TreeSet;

/**
 * Paul Cameron
 * 12/3/2020
 * Lab 12
 * 
 * Utility to process a document to into sorted set of unique words
 * @author student name
 */
public class UniqueWords {
    /**
     * Read file one line at a time
     * Break input String into words
     * Store unique words sorted into alphabetic order
     * @param myfile input file
     * @return sorted set of unique words
     */
    public static Set<String> processDocument(File myfile) throws IOException{
        BufferedReader br = new BufferedReader(new FileReader(myfile));
        String line = null;
        TreeSet<String> set = new TreeSet<>();

        while((line = br.readLine()) != null ){
            for(String token : tokenize(line)){
                set.add(token);
            }
        }
        br.close();
        return set;
    }

    /**
     * Remove all punctuation and numbers from String
     * Tokenize - break into individual words
     * Convert all words to lower case
     * @param str initial non-null String
     * @return array of words from initial String
     */
    public static String[] tokenize(String str) {
        str = str.replaceAll("[^a-zA-Z \n]"," ");
        String[] tok = str.split(" ");
        for (int i=0; i<tok.length; ++i) {
            tok[i] = tok[i].toLowerCase();
        }
        return tok;
    }
}