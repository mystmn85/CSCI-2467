package edu.cscc;

import java.util.*;

// Bob Platt, 6/21/2017
// Compute fine for speeding
public class Main {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int vehSpeed, speedLimit, fine, diff;
        System.out.print("Enter speed limit: ");
        speedLimit = input.nextInt();
        System.out.print("Enter vehicle speed: ");
        vehSpeed = input.nextInt();
        diff = vehSpeed - speedLimit;

        if (diff <= 0) {
            fine = 0;
        } else if (diff < 10) {
            fine = 25;
        } else if (diff < 20) {
            fine = 50;
        } else if (diff < 30) {
            fine = 100;
        } else if (diff < 40) {
            fine = 250;
        } else {
            fine = 500;
        }

        if (fine == 0) {
            System.out.println("Driver was not speeding");
        } else {
            System.out.println("The fine for driving " + diff +
                    " MPH over the speed limit is $" + fine);
        }
    }
}
