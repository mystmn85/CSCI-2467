package edu.cscc;

import java.util.Scanner;
import java.util.HashMap;
// Paul Cameron
// 12/6/2020
// This application takes a CSV file, sorts for a name and outputs additional information from that row.

public class Main {
    private final static String CENSUSDATAFNAME = "Surnames_2010Census.csv";
    public static Scanner askUser = new Scanner(System.in);

    public static void main(String[] args) {
        HashMap<String, Surname> loadMapping = Census.loadCensusData(CENSUSDATAFNAME);

        if (askUser != null) {
            while(true){
                System.out.println("Enter a surname (or quit to end)");
                String userinput = askUser.nextLine();

                if(userinput.equals("quit")){
                    System.out.println("Exiting program");
                    System.exit(0);
                }else{
                    userinput = userinput.toUpperCase();
                
                    if(loadMapping.containsKey(userinput))
                    {
                        System.out.println(
                            "Surname: "+loadMapping.get(userinput).getName()
                            +" rank: "+loadMapping.get(userinput).getRank()
                            +" count: "+loadMapping.get(userinput).getCount()
                            +" proportion: "+loadMapping.get(userinput).getProportion());
                    }else{
                        System.out.println("Surname: "+userinput+" not found");
                    }
                }
            }
        }
    }
}
