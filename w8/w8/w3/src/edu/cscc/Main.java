package edu.cscc;

import java.util.Scanner;

/*
Paul Cameron
10/27/2020
The application inputs the users weight in lbs and height in inches). It then converts the lbs to kgs and the inches
to meters and outputs the BMI for that individual. It also includes input validation.

 */
public class Main {
    private static Scanner input = new Scanner(System.in);
    private static final String INPUT_ERROR = "Input is not valid, try again";

    public static void main(String[] args) {
        double lbs, inches, meters, kgs, bmi;
        String classification;

        System.out.println("Calculate BMI");

        lbs = inputWeightAndHeight("Enter weight (lbs):");
        inches = inputWeightAndHeight("Enter height (inches):");

        kgs = convertToKilograms(lbs);
        meters = convertToMeters(inches);

        bmi = calcBMI(meters, kgs);
        System.out.println("Your BMI is " + bmi);

        classification = bmiClassification(bmi);
        System.out.println("Your BMI classification is " + classification);
    }

    // TODO add your methods here (make them static)
    private static double inputWeightAndHeight(String msg) {
        double check;

        do {
            System.out.println(msg);

            try{
                check = Double.parseDouble(input.next());

            }catch(Exception e) {
                check = Double.parseDouble("0");
            }

            if(check <= 0) {
                System.out.println(INPUT_ERROR);
            }

        } while (check <= 0);

        return check;
    }

    private static double convertToKilograms(double lbs) {
        return lbs / 2.2046;
    }

    private static double convertToMeters(double inches) {
        return inches / 39.37;
    }

    private static double calcBMI(double meters, double kgs) {
        return kgs / Math.pow(meters, 2);
    }

    private static String bmiClassification(double bmi) {
        String classification = "";

        if (bmi < 18.5) {
            classification = "Underweight: ";
        } else if (bmi >= 18.5 && bmi < 25.0) {
            classification = "Normal";
        } else if (bmi >= 25.0 && bmi < 30.0) {
            classification = "Overweight";
        } else if (bmi > 30.0) {
            classification = "Obese";
        }
        return classification;
    }
}
