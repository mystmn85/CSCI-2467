package edu.cscc;

public class PersonAddress extends Address {
    private String personName;

    //constructor
    public PersonAddress(String personName, String streetAddress, String city, String state, String zip) {
        super(streetAddress, city, state, zip);
        this.personName = personName;
    }

    //Getter
    public String getPersonName() {
        return personName;
    }

    //Setter
    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public void printLabel() {
        System.out.println(
                this.personName + "\n ");
    }
}
